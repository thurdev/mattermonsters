import React from "react"
import { Context } from "./Context"
import html2canvas from "html2canvas"
import logo from "../images/logo.svg"
import launch from "../images/launch.svg"
import balloons from "../images/Balloons@4x.svg"

function ReleaseForm() {
  const { myContext, updateMyContext } = React.useContext(Context)

  const calledOnce = React.useRef(false)

  React.useEffect(() => {
    if (calledOnce.current && myContext.page !== 2) {
      return
    }
    window.MktoForms2.loadForm("//forms.mattermost.com", "161-FBE-733", 2085)
    calledOnce.current = true

    const submitBtn = document.querySelector(
      "#mktoForm_2085 > div.mktoButtonRow > span > button"
    )

    function handleClick() {
      console.log("clicked!")
    }

    window.MktoForms2.whenReady(function (form) {
      form.setValues({
        matterMonsterB64: window.matterMonster,
      })
      form.onSuccess(function (vals, thanksURL) {
        const newContext = Object.assign({}, myContext)
        newContext.page = 3
        updateMyContext(newContext)
        return false
      })
    })

    if (submitBtn) {
      console.log("added the listener")
      submitBtn.addEventListener("click", handleClick)
    }
  }, [calledOnce, myContext, myContext.page, updateMyContext])

  new FontFace(
    "Permanent Marker",
    "url(https://fonts.gstatic.com/s/permanentmarker/v16/Fh4uPib9Iyv2ucM6pGQMWimMp004La2Cf5b6jlg.woff2)"
  )
    .load()
    .then((font) => {
      document.fonts.add(font)
    })

  const releaseMonster = () => {
    html2canvas(document.querySelector("#canvas")).then((monster) => {
      const newContext = Object.assign({}, myContext)
      newContext.monsterName = document.querySelector("#monster-name").value
      newContext.page = 2
      updateMyContext(newContext)

      //document.body.appendChild(monster)

      const photo = document.createElement("canvas")
      photo.width = 422
      photo.height = 503
      const ctx = photo.getContext("2d")

      ctx.drawImage(document.querySelector("#polaroid"), 0, 0) // draw polaroid frame
      ctx.drawImage(monster, 10, 10, 400, 400) // draw monster inside frame
      ctx.drawImage(document.querySelector("#mm-logo"), 300, 10, 100, 100)

      let monsterName = newContext.monsterName

      if (monsterName.length < 8) {
        ctx.font = "bold 48px Permanent Marker"
        ctx.rotate((20 * Math.PI) / 180)
        ctx.fillText(monsterName, 150, 360)
        ctx.restore()
      } else if (monsterName.length < 12) {
        ctx.font = "bold 30px Permanent Marker"
        ctx.rotate((-20 * Math.PI) / 180)
        ctx.fillText(monsterName, -40, 490)
        ctx.restore()
      } else {
        ctx.font = "bold 25px Permanent Marker"
        ctx.fillText(monsterName, 40, 460)
      }

      //document.body.appendChild(photo)

      window.matterMonster = crypto.randomUUID()

      const body = new FormData()
      body.append("monsterName", monsterName)
      body.append("monsterId", window.matterMonster)
      body.append("monsterAvatar", JSON.stringify(monster.toDataURL()))
      body.append("monsterPolaroid", JSON.stringify(photo.toDataURL()))
      body.append(
        "monsterJson",
        JSON.stringify({
          mouth: newContext.mouthIndex,
          eyes: newContext.eyesIndex,
          body: newContext.bodyIndex,
          clothing: newContext.clothingIndex,
          accessory: newContext.accessoryIndex,
        })
      )

      const REMOTE = "https://mattermonsters.andrewzigler.com:4000"
      fetch(`${REMOTE}/api/v1/mattermost/release`, {
        method: "POST",
        body,
      })
    })
  }

  const checkFields = () => {
    if (document.querySelector("#monster-name").value) {
      const newContext = Object.assign({}, myContext)
      newContext.releasable = true
      updateMyContext(newContext)
    } else {
      if (myContext.releasable) {
        const newContext = Object.assign({}, myContext)
        newContext.releasable = false
        updateMyContext(newContext)
      }
    }
  }

  if (myContext.page === 1) {
    return (
      <div id="release-form">
        <img src={logo} className="logo" alt="Mattermost logo" />
        <h1 className="heading">Build your Mattermonster</h1>
        <h2 className="subheading">
          Mattermonsters are running amok in Mattermost! Create and release your
          Mattermonster now.
        </h2>
        <input
          id="monster-name"
          type="text"
          placeholder="Monster's Name*"
          onChange={checkFields}
        />
        <button
          className="release"
          disabled={myContext.releasable ? false : true}
          onClick={myContext.releasable ? releaseMonster : () => {}}
        >
          <img src={launch} className="launch" alt="Launch icon" />
          Release to Community Server
        </button>
      </div>
    )
  } else if (myContext.page === 2) {
    return (
      <div id="release-form">
        <img src={logo} className="logo" alt="Mattermost logo" />
        <h1 className="heading">It&apos;s Alive!</h1>
        <h2 className="subheading">
          Your Mattermonster has been released on the{" "}
          <a
            target="_blank"
            href="https://community.mattermost.com/core/channels/mattermonsters"
            rel="noreferrer"
          >
            Community server
          </a>
          ! Enter your email below and we&apos;ll capture and send it to you.
        </h2>
        <span id="marketo">
          <form id="mktoForm_2085" />
        </span>
      </div>
    )
  } else if (myContext.page === 3) {
    return (
      <div id="release-form">
        <img src={logo} className="logo" alt="Mattermost logo" />
        <h1 className="heading">Success!</h1>
        <h2 className="subheading">
          Check your inbox for your Mattermonster in the coming days, check it
          out on our the{" "}
          <a
            target="_blank"
            href="https://community.mattermost.com/core/channels/mattermonsters"
            rel="noreferrer"
          >
            Community server
          </a>
          , or{" "}
          <a
            href="/#"
            onClick={() => {
              const newContext = Object.assign({}, myContext)
              newContext.page = 1
              updateMyContext(newContext)
            }}
          >
            build another one
          </a>
          .
        </h2>
        <img alt="Balloons" src={balloons} style={{ width: "111px" }} />
      </div>
    )
  }
}

export { ReleaseForm }
