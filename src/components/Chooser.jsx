import React from "react"
import { Context } from "./Context"

function Chooser({ label, images, indexPropName, imagePropName }) {
  const { myContext, updateMyContext } = React.useContext(Context)

  const maxIndex = images.length

  const upFunc = () => {
    const newContext = Object.assign({}, myContext)
    if (newContext[indexPropName] + 1 <= maxIndex) {
      newContext[indexPropName] = newContext[indexPropName] + 1
      newContext[imagePropName] = images[newContext[indexPropName] - 1]
    } else {
      newContext[indexPropName] = 1
      newContext[imagePropName] = images[0]
    }
    updateMyContext(newContext)
    console.log(myContext)
  }

  const downFunc = () => {
    const newContext = Object.assign({}, myContext)
    if (newContext[indexPropName] - 1 !== 0) {
      newContext[indexPropName] = newContext[indexPropName] - 1
      newContext[imagePropName] = images[newContext[indexPropName] - 1]
    } else {
      newContext[indexPropName] = maxIndex
      newContext[imagePropName] = images[maxIndex - 1]
    }
    updateMyContext(newContext)
    console.log(myContext)
  }

  return (
    <div className="chooser">
      <button onClick={downFunc} disabled={myContext.page !== 1} className={myContext.page !== 1 ? "disabled" : ""}>&lt;</button>
      <span>{label}</span>
      <button onClick={upFunc} disabled={myContext.page !== 1} className={myContext.page !== 1 ? "disabled" : ""}>&gt;</button>
    </div>
  )
}

export { Chooser }
