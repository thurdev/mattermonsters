import img1 from "../images/Z1_Mouth/Mouth01@2x.png"
import img2 from "../images/Z1_Mouth/Mouth02@2x.png"

const images = [img1, img2]

export const mouthData = {
  label: "mouth",
  images,
  zIndex: 1,
}
