import img1 from "../images/Z5_Body/Body01@2x.png"
import img2 from "../images/Z5_Body/Body02@2x.png"

const images = [img1, img2]

export const bodyData = {
  label: "body",
  images,
  zIndex: 5,
}
