import React from "react"
import ReactDOM from "react-dom/client"
import { MonsterMaker } from "./components/MonsterMaker"
import { ContextProvider } from "./components/Context"
import "./styles.css"

const root = ReactDOM.createRoot(document.getElementById("root"))
root.render(
  <React.StrictMode>
    <ContextProvider>
      <MonsterMaker />
    </ContextProvider>
  </React.StrictMode>
)
